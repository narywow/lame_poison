#define _USE_MATH_DEFINES 

#include <iostream>
#include <fstream>
#include <string>
#include <cmath>

using namespace std;

inline double gu_x_0(double y)
{
	return 0;
}
inline double gu_x_n(double y)
{
	//������ 1,3
	return 0;
	
	//������ 2
	//return 2 * cos(4 * M_PI * y);
}
inline double gu_y_0(double x)
{
	//������ 1
	//return 0;

	//������ 2
	//return 2 * x;

	//������ 3
	return 20;
}
inline double gu_y_n(double x)
{
	//������ 1
	//return 0;

	//������ 2
	//return 2 * x;

	//������ 3
	return 20;
}
double p(double x, double y, int i, int j)
{
	//������ 1
	/*if (i == 4 && j == 4) 
	{
		return -200;
	}
	if (i == 20 && j == 12) 
	{
		return -200;
	}
	if (i == 25 && j == 25) 
	{
		return 200;
	}
	return 0;*/

	//������ 2
	//return 0.05;

	//������ 3
	return 0.01;
}

class Lame
{
private:
	double nu = 0.3;
	double b = 3e-2;
	double a = 5e-2;
	double p = 20e+6;
	double e = 2e+11;
	int N;
	double h;
	double* uCurr;
	double pogr;

	void progonka(int N, double* A, double* B, double* C, double* F, double* X)
	{
		double* alpha, * beta;
		alpha = new double[N + 1];
		beta = new double[N + 1];

		alpha[0] = -C[0] / B[0];
		beta[0] = F[0] / B[0];

		for (int i = 1; i < N + 1; i++)
		{
			alpha[i] = -C[i] / (B[i] + A[i] * alpha[i - 1]);
			beta[i] = (F[i] - A[i] * beta[i - 1]) / (B[i] + A[i] * alpha[i - 1]);
		}

		X[N] = beta[N];

		for (int i = N - 1; i >= 0; i--)
		{
			X[i] = alpha[i] * X[i + 1] + beta[i];
		}

		delete[] alpha;
		delete[] beta;

	}

	inline double anal_sol(double r)
	{
		return a * a * b * b * p * (nu + 1) / e / (a * a - b * b) / r - (nu - 1) * b * b * p * r / e / (a * a - b * b);
	}

	void solve()
	{
		double* arrA = new double[N + 1];
		double* arrB = new double[N + 1];
		double* arrC = new double[N + 1];
		double* arrF = new double[N + 1];

		arrA[0] = 0;
		arrB[0] = (nu / b - 1. / h);
		arrC[0] = 1. / h;
		arrF[0] = -p * (1 - nu * nu) / e;

		arrA[N] = -1. / h;
		arrB[N] = 1. / h + nu / a;
		arrC[N] = 0;
		arrF[N] = 0;

		for (int x = 1; x < N; x++)
		{
			arrA[x] = 1. / h / h - 1. / 2. / h / (b + h * x);
			arrB[x] = -2. / h / h - 1. / pow(b + h * x, 2);
			arrC[x] = 1. / h / h + 1. / 2. / h / (b + h * x);
			arrF[x] = 0;
		}
		progonka(N, arrA, arrB, arrC, arrF, uCurr);

		delete[] arrA;
		delete[] arrB;
		delete[] arrC;
		delete[] arrF;
	}
public:
	Lame(int N, double a, double b, double nu, double p, double e)
	{
		this->a = a;
		this->b = b;
		this->nu = nu;
		this->p = p;
		this->e = e;
		this->N = N;
		uCurr = new double[N + 1];
		h = (a - b) / (double)N;
		solve();
	}

	Lame(int N)
	{
		this->N = N;
		uCurr = new double[N + 1];
		h = (double)(a-b)/ (double)N;
		solve();
	}
	
	double nevyazka() 
	{
		double pogr_abs = 0;
		for (int i = 0; i < N; i++)
		{
			double pogr_temp=abs(anal_sol(b+i*h)-uCurr[i]);
			if (pogr_abs < pogr_temp)
			{
				pogr_abs = pogr_temp;
			}
			//cout << pogr_temp << endl;
		}
		pogr = pogr_abs;
		return pogr;
	}

	void solve_to_file(string file)
	{
		string tmp_file = file + "_" + to_string(N) + ".txt";
		ofstream fout(tmp_file);

		for (int x = 0; x <= N; x++)
		{
			fout << b + x * h << '\t' << uCurr[x] <<"\t"<<anal_sol(b+x*h)<< endl;
		}
		fout.close();
	}

	void setN(int N)
	{
		this->N = N;
		delete[] uCurr;
		uCurr = new double[N + 1];
		h = (double)(a - b) / (double)N;
		solve();
	}

	~Lame()
	{
		delete[] uCurr;
	}
	
};


class Poisson
{
private:
	double(*GU1)(double);
	double(*GU2)(double);
	double(*GU3)(double);
	double(*GU4)(double);
	double(*P)(double, double, int, int);
	int N;
	int M;
	double eps;
	double lx = 1;
	double ly = 1;
	double dx;
	double dy;
	double** sol;
	int iterations=0;


	void SORMethod()
	{
		double** uPrev = new double* [N + 1];
		double** uCurr = new double* [N + 1];
		for (int i = 0; i < N + 1; i++)
		{
			uPrev[i] = new double[M + 1];
			uCurr[i] = new double[M + 1];
		}
		for (int i = 0; i < N + 1; i++)
		{
			uCurr[i][0] = GU3(i * dx);
			uCurr[i][N] = GU4(i * dx);
		}
		for (int i = 0; i < M + 1; i++)
		{
			uCurr[0][i] = GU1(i * dy);
			uCurr[N][i] = GU2(i * dy);

		}
		for (int i = 0; i < N + 1; i++)
		{
			for (int j = 0; j < N + 1; j++)
			{
				uPrev[i][j] = 0;
			}
		}

		double alpha = 2. / dx / dx + 2. / dy / dy;
		double beta = 1. / dx / dx;
		double gamma = 1. / dy / dy;
		double lyambda_min = 2. * (dy * dy * pow(sin(M_PI * dx / 2. / lx), 2) + dx * dx * pow(sin(M_PI * dy / 2. / ly), 2)) / (dx * dx + dy * dy);
		double omega = 2. / (1. + sqrt(lyambda_min * (2 - lyambda_min)));

		double pogr;
		int iterations = 0;

		do
		{
			iterations++;
			pogr = 0.;
			for (int i = 1; i < N; i++)
			{
				for (int j = 1; j < M; j++)
				{

					uCurr[i][j] = (1. - omega) * uPrev[i][j] + omega * (gamma * uCurr[i][j - 1] + beta * uCurr[i - 1][j] + gamma * uPrev[i][j + 1] + beta * uPrev[i + 1][j] + p(i * dx, j * dy, i, j)) / alpha;
				}

			}
			for (int i = 0; i < N + 1; i++)
			{
				for (int j = 0; j < N + 1; j++)
				{
					if (abs(uPrev[i][j] - uCurr[i][j]) > pogr)
					{

						pogr = abs(uPrev[i][j] - uCurr[i][j]);
					}
					uPrev[i][j] = uCurr[i][j];
				}
			}

		} while (pogr > eps);

		for (int i = 0; i < N + 1; i++)
		{
			delete[] uPrev[i];
		}
		delete[] uPrev;

		sol = uCurr;
		this->iterations = iterations;
		//cout <<"done(SOR)" << endl;

	}

public:
	Poisson(int N,int M,double(*gu1)(double), double(*gu2)(double), double(*gu3)(double), double(*gu4)(double), double(*p)(double, double, int, int), double eps)
	{
		GU1 = gu1;
		GU2 = gu2;
		GU3 = gu3;
		GU4 = gu4;
		P = p;
		this->eps = eps;
		this->M = M;
		this->N = N;
		dx = lx / N;
		dy = ly / M;
		SORMethod();
		//cout << "done(Constructor)" << endl;
	}
	
	void setMN(int N, int M)
	{
		this->M = M;
		this->N = N;
		dx = lx / N;
		dy = ly / M;
		SORMethod();
		//cout << "done(set)" << endl;

	}
	void solve_to_file(string file)
	{
		string tmp_file = file + "_" + to_string(N) + "_" + to_string(M) + ".txt";
		ofstream fout(tmp_file);

		for (int i = -1; i <= N; i++)
		{
			for (int j = -1; j <= M; j++)
			{
				if (i == -1 && j == -1)
				{
					fout << 0 << '\t';
				}
				else if (i == -1)
				{
					fout << j * dy << '\t';
				}
				else if (j == -1)
				{
					fout << i * dx << '\t';
				}
				else
				{
					fout << sol[i][j] << '\t';

				}

			}
			fout << std::endl;

		}

		fout.close();
		//cout << "done(file)" << endl;

	}
	int getIterations()
	{
		return iterations;
	}
};





int main()
{
	setlocale(0, "ru_ru");

	int N = 10;


	Lame lame(N);
	lame.solve_to_file("lame");
	cout << "����������� ���������� ����� ��� ��������� ���� � ���-��� ����� " << N << " : " << lame.nevyazka() << endl;
	N = 50;
	lame.setN(N);
	lame.solve_to_file("lame");
	cout << "����������� ���������� ����� ��� ��������� ���� � ���-��� ����� " << N << " : " << lame.nevyazka() << endl;
	N = 100;
	lame.setN(N);
	lame.solve_to_file("lame");
	cout << "����������� ���������� ����� ��� ��������� ���� � ���-��� ����� " << N << " : " << lame.nevyazka() << endl;

	int M = 30;
	N = 30;
	double eps = 1e-5;

	Poisson poison(N, M, gu_x_0, gu_x_n, gu_y_0, gu_y_n, p, eps);
	poison.solve_to_file("poison");
	cout << "\n���-�� �������� ��� ������ SOR � ������ � ���-��� ����� " << N << "x" << M << " � ������������ " <<eps<<" : "  << poison.getIterations() << endl;

	M = 60;
	N = 60;
	poison.setMN(N, M);
	poison.solve_to_file("poison");

	cout << "\n���-�� �������� ��� ������ SOR � ������ � ���-��� ����� " << N << "x" << M << " � ������������ " <<eps<<" : "  << poison.getIterations() << endl;

	M = 100;
	N = 100;
	poison.setMN(N, M);
	poison.solve_to_file("poison");

	cout << "\n���-�� �������� ��� ������ SOR � ������ � ���-��� ����� " << N << "x" << M << " � ������������ " <<eps<<" : "  << poison.getIterations() << endl; 

}
